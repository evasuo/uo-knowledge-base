> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/fonts.html
> 

## Fonts

**Data File:** Fonts.mul  

This file has no indexer. Directly after one character set ends, another begins.  

#### Character Set:

```csharp
 byte header; // Unknown

 List Character[224]: // First 32 characters are not included in the set
   byte width;
   byte height;
   byte header; // Unknown
   short[width * height] imageColors;
```

The imageColor array is loaded top to bottom, left to right. A color of 0x0000 should be treated as transparent.  

**See Also:**  
 - [UO Color Format](colors.md "Colors")  
