> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/radarcol.html
> 

## Radar Colors

**Data File:** RadarCol.mul  

#### Data:

```csharp
 short[0x8000] colors;
```

This file simply contains color values for each tile.  

**See Also:**  
 - [UO Color Format](colors.md "Colors")  
 - [Maps](maps.md "Maps")  
</span>
