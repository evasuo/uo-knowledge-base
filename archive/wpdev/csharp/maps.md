> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/maps.html
> 

## Maps

**Data File:** Map<n>.mul  

This file has no indexer. Directly after one block ends, another begins.  

#### Block:

```csharp
int header; // Unknown
Cells...
```

Each block contains 64 cells, treated as an 8x8 matrix loaded left to right, top to bottom.

#### Cell:

```csharp
short TileID;
sbyte Z;
```

You can seek to a certain block, by knowing the map height.  

- For Map0.mul, the block width and height is 768x512.  
- For Map2.mul, it's 288x200.  

The algorithm used to find the block lookup is:  

```csharp
((XBlock * BlockHeight) + YBlock) * 196
```

From there, the offset to a certain cell is:  

```csharp
((CellY * 8) + CellX) * 3
```

**See Also:**  
 - [Statics](statics.md "Statics")  
 - [Radar Colors](radarcol.md "Radarcol")  
