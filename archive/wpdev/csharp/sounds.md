> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/sounds.html
> 

## Sounds

**Data File:** Sound.mul  
**Index File:** SoundIdx.mul  

#### Data:

```csharp
char[20] fileName; // Original *.wav file name
byte[20] header; // Unknown
byte[length - 40] waveData;
```

waveData can be written out to a .wav file's "data" chunk, after writing out the "RIFF" and "WAVEfmt " chunks.

The wave files have PCM 22.050 kHz, 16-Bit Mono format.  

**See Also:**  
 - [Index Files](indexes.md "Index")
 
