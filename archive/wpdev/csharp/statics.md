> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/statics.html
> 

## Statics

**Data File:** Statics<n>.mul  
**Index File:** StaIdx<n>.mul  

#### Block:

```csharp
List Items[length / 7]:
   short TileID;
   byte X; // Ranges from 0 to 7
   byte Y; // Ranges from 0 to 7
   sbyte Z;
   short Unknown; // At one point this was the hue, but doesn't appear to be used anymore
```

The list needs to be sorted.  

To find the index of a certain block, you need to know the map height.  

- For Statics0.mul, the block width and height is 768x512.  
- For Statics2.mul, it's 288x200.  

So, you seek in the index file:

```csharp
((XBlock * BlockHeight) + YBlock) * 12
```

**See Also:**  
 - [Maps](maps.md "Maps")  
 - [Radar Colors](radarcol.md "Radarcol")  
