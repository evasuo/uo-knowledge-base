> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/animdata.html
> 

## AnimData

**Data File:** AnimData.mul  

This file has no indexer. Directly after one block ends, another begins.  

#### Block


```csharp
int header; // Unknown
Entries...
```

Each block contains eight entries.

#### Entry

```csharp
sbyte[64] frameData; // This could be less than 64 bytes, I recall seeing some garbage data near the end.
byte unknown;
byte frameCount;
byte frameInterval; // Move to the next frame every (frameInterval * 50) milliseconds.
byte frameStart; // It is believed this is the delay before the animation starts (frameStart * 50ms).
```
