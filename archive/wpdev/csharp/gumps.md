> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/gumps.html
> 

## Gumps

**Data File:** GumpArt.mul  
**Index File:** GumpIdx.mul  

You can compute the following values from the Extra field in the index file.  

#### Sizes:

```csharp
short width = ((extra >> 16) & 0xFFFF);
short height = (extra & 0xFFFF);
```

#### Data:

```csharp
int[height] lookupTable;
RLE Data...
```

RLE stands for "run-length encoding", it is a simple form of image compression.

This implementation of the RLE compression is treated as 32-bit values, broken up into two 16-bit values

#### RLE Packet:

<table border="1">

<thead>

<tr>

<th>1F</th>

<th>1E</th>

<th>1D</th>

<th>1C</th>

<th>1B</th>

<th>1A</th>

<th>19</th>

<th>18</th>

<th>17</th>

<th>16</th>

<th>15</th>

<th>14</th>

<th>13</th>

<th>12</th>

<th>11</th>

<th>10</th>

<th>0F</th>

<th>0E</th>

<th>0D</th>

<th>0C</th>

<th>0B</th>

<th>0A</th>

<th>09</th>

<th>08</th>

<th>07</th>

<th>06</th>

<th>05</th>

<th>04</th>

<th>03</th>

<th>02</th>

<th>01</th>

<th>00</td>

</tr>
</thead>
<tbody>
<tr>
<td colspan="16">Run</td>
<td colspan="16">Color</td>
</tr>
</tbody>
</table>

The Color field indicates which color should be repeated. The Run field represents the length of pixels:  

```csharp
<for ( int i = 0; i < Run; i++ )
{
   SetPixel( x + i, y, Color );
}
x += Run;
```

**See Also:**  
 - [UO Color Format](colors.md "Colors")  
 - [Index Files](indexes.md "Index")  
