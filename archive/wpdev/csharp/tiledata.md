> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/tiledata.html
> 

## TileData

**Data File:** TileData.mul

This file has no indexer. Directly after one block ends, another begins.

There are two types of blocks; land and static.

There are 512 land blocks, followed by any number of static blocks

#### Land Block:

```csharp
int header; // Unknown

List Land Entries[32]:
   int flags; // See below
   short TextureID; // If 0, the land tile doesn't have a texture
   char[20] name;
```

#### Static Block:

```csharp
int header; // Unknown

List Static Entries[32]:
   int flags; // See below
   byte weight; // 255 if not moveable
   byte quality; // See below
   short unknown1;
   byte unknown2;
   byte quantity; // See below
   short animation; // The BodyID of the item's animation
   byte unknown3;
   byte hue; // Unknown use, perhaps colored lighting?
   byte unknown4;
   byte unknown5; // Known in some circles as "Value"
   byte height;
   char[20] name;
```

|Flags||
|-|-|
|0x00000001|Background|
|0x00000002|Weapon|
|0x00000004|Transparent|
|0x00000008|Translucent|
|0x00000010|Wall|
|0x00000020|Damaging|
|0x00000040|Impassable|
|0x00000080|Wet|
|0x00000100|Unknown1|
|0x00000200|Surface|
|0x00000400|Bridge|
|0x00000800|Generic|
|0x00001000|Window|
|0x00002000|NoShoot|
|0x00004000|ArticleA|
|0x00008000|ArticleAn|
|0x00010000|Internal|
|0x00020000|Foliage|
|0x00040000|PartialHue|
|0x00080000|Unknown2|
|0x00100000|Map|
|0x00200000|Container|
|0x00400000|Wearable|
|0x00800000|LightSource|
|0x01000000|Animation|
|0x02000000|NoDiagonal|
|0x04000000|Unknown3|
|0x08000000|Armor|
|0x10000000|Roof|
|0x20000000|Door|
|0x40000000|StairBack|
|0x80000000|StairRight|


If Weapon, Quantity is Weapon Class.  

If Armor, Quantity is Armor Class.  

If Wearable, Quality is Layer.  

If Light Source, Quality is LightID  

If Container, Height is "Contains" (Something determining how much the container can hold?)  
