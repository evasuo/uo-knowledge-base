> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/skills.html
> 

## Skills

**Data File:** Skills.mul  
**Index File:** Skills.idx  

<font face="Courier New">

#### Data:

```csharp
bool isAction; // Does the skill have an action button?
char[length - 1] name;
```

**See Also:**  
 - [Index Files](indexes.md "Index")  
 - [Skill Groups](skillgroups.md "Skillgroups")  
