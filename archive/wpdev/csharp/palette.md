> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/palette.html
> 

## Palette

**Data File:** Palette.mul  

#### Data:

```csharp
List RGBValues[256]:
   byte R;
   byte G;
   byte B;
```

This file simply contains a palette of 24-bit RGB color values.
