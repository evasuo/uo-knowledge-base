> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/animations.html
> 

## Animations

**Data File:** Anim.mul  
**Index File:** Anim.idx  


#### Data:

```csharp
short[0x100] palette;
int frameCount;
int[frameCount] lookupTable;
Frame Data...
```

To find the data for a frame, seek to 0x200 + lookupTable[frame].

#### Frame Data:

```csharp
short centerX;
short centerY;
short width;
short height;
Pixel Data...
```

Pixel data comes in chunks. If the current chunk header is 0x7FFF7FFF, the image is completed.

#### Chunks:

```csharp
int header;
byte[XRun] palettePixels; // See description below for XRun
```

#### Header Bit Masks:

<table>
<thead>
<tr>
<th>1F</th>
<th>1E</th>
<th>1D</th>
<th>1C</th>
<th>1B</th>
<th>1A</th>
<th>19</th>
<th>18</th>
<th>17</th>
<th>16</th>
<th>15</th>
<th>14</th>
<th>13</th>
<th>12</th>
<th>11</th>
<th>10</th>
<th>0F</th>
<th>0E</th>
<th>0D</th>
<th>0C</th>
<th>0B</th>
<th>0A</th>
<th>09</th>
<th>08</th>
<th>07</th>
<th>06</th>
<th>05</th>
<th>04</th>
<th>03</th>
<th>02</th>
<th>01</th>
<th>00</th>
</tr>
</thead>

<tbody>
<tr>
<td colspan="10"><a title="Bitmask: 0xFFC00000">YOffset</a></td>
<td colspan="10"><a title="Bitmask: 0x3FF000">XOffset</a></td>
<td colspan="12"><a title="Bitmask: 0xFFF">XRun</a></td>
</tr>
</tbody>
</table>

XOffset and YOffset are relative to centerX and centerY.

XRun indicates how many pixels are contained in this line.

XOffset and YOffset are signed, so we need to compensate for that:

```csharp
XOffset = (XOffset ^ 0x200) - 0x200
YOffset = (YOffset ^ 0x200) - 0x200
```

**See Also:**  

 - [UO Color Format](colors.md "Colors")  
 - [Index Files](indexes.md "Index")  
