> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/unifont.html
> 

## Unicode Fonts

**Data File:** UniFont[n].mul  

This file begins with a lookup table:  

#### Lookup Table:

```csharp
 int[0x10000] lookupTable;
```

You can then find the data by seeking to lookupTable[charID]

#### Data:

```csharp
byte xOffset;
byte yOffset;
byte xWidth;
byte yHeight;
Scanline Data...
```

Scanlines are padded on a byte-boundary; for each scanline, you will have ((xWidth - 1) / 8) + 1 bytes of data:  

#### Scanline Byte:

<table border="1">

<tbody>

<tr>

<td>7</td>

<td>6</td>

<td>5</td>

<td>4</td>

<td>3</td>

<td>2</td>

<td>1</td>

<td>0</td>

</tr>

<tr>

<td colspan="8">8 pixels at 1-bpp</td>

</tr>

</tbody>

</table>

Bits are loaded high to low; bit 7 (mask: 0x80) would be the first pixel.  

If the bit value is 0, color is transparent, else, color is forecolor.  

**See Also:**  
 - [SJIS To UniFont Table](sjis2uni.md "Sjis2uni")  
