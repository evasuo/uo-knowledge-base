> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/colors.html
> 

## UO Colors

UO's graphics are all 16-bit 555 color: 

#### Data:

```csharp
 short color;
 ```

#### Color Bit Masks:


<table>
<thead>
<tr>
<th>F</th>
<th>E</th>
<th>D</th>
<th>C</th>
<th>B</th>
<th>A</th>
<th>9</th>
<th>8</th>
<th>7</th>
<th>6</th>
<th>5</th>
<th>4</th>
<th>3</th>
<th>2</th>
<th>1</th>
<th>0</th>
</tr>
</thead>
<tbody>
<tr>
<td colspan="1"><a title="Bitmask: 0x8000"></a></td>
<td colspan="5" bgcolor="#C80000"><font color="#FFFFFF"><a title="Bitmask: 0x7C00">Red</a></font></td>
<td colspan="5" bgcolor="#00C800"><font color="#FFFFFF"><a title="Bitmask: 0x3E0">Green</a></font></td>
<td colspan="5" bgcolor="#0000C8"><font color="#FFFFFF"><a title="Bitmask: 0x1F">Blue</a></font></td>
</tr>
</tbody>
</table>

_From Archives_

![Color Bit Mask From Archive Source](images/color/color-bit-mask.png "Color Bit Mask From Archive Source")

Bit 15 is normally used for transparency, but doesn't appear to be so in UO.  
