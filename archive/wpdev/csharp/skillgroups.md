> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/skillgroups.html
> 

## Skill Groups

**Data File:** SkillGrp.mul  

#### Data:

```csharp
 int groupCount;

List Groups[groupCount]:
   char[17] groupName;
int[?] table; // Contains which group a skill is in.
```

The table array extends to the end of the file.

To determine which group a skill is in, simply use table[SkillID].  

**See Also:**  
 - [Skills](skills.md "Skills")  
</span>
