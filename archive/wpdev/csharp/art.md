> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/art.html
> 

## Art

**Data File:** Art.mul  
**Index File:** ArtIdx.mul  

There are three types of art images; land, static, and UO alpha

Land images are of flat terrain.

Static images are of items (walls, equipment, food, etc)

UO alpha images are a sort of animated characters, they used these instead of Anim.mul in the UO alpha.  

Static and UO alpha images share the same format, while Land images use another.  

#### Land:

```csharp
short[1024] imageColors;
```

Land images are fixed size at 44x44.  

They also have fixed transparent locations, and the format is optimized for this.  

To read the first 22 lines, you first initialize X to 22, Y to 0, and LineWidth to 2  

Then repeat the following 22 times:  

```
Decrease X by 1
Read and Draw (LineWidth) number of pixels
Increase Y by 1
Increase LineWidth by 2
``` 

For the last 22 lines, do the following:

```
Read and Draw (LineWidth) number of pixels
Increase X by 1
Increase Y by 1
Decrease LineWidth by 2
``` 

The resulting image is the diamond shaped tile.  

#### Statics:

```csharp
int header; // Unknown
short width;
short height;
short[height] lookupTable;
Offset-Run data...
```

The lookup table is offset from the data start, and treats the data by 16-bits.  

To get the proper byte offset, you need to add 4 + height, then multiply by two.  

The static images are compressed with an offset-run encoding.  

#### Offset-Run data:

```csharp
short xOffset;
short xRun;
short[xRun] runColors;
```

 If xOffset is 0 and xRun is 0, the line has been completed. If not, continue reading the chunks.  

Processing the Offset-Run data is simple:  

```
Increase X by xOffset
Foreach xRun
Read and Draw the next pixel
Increase X by 1
End Foreach
```

When the line is completed, simply reset X to 0, increase Y, and seek to the next lookup in the lookupTable array and continue.  

**See Also:**  
 - [UO Color Format](colors.md "Colors")  
 - [Index Files](indexes.md "Index")  
