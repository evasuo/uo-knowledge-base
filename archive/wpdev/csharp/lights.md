> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/lights.html
> 

## Lights

**Data File:** Light.mul  
**Index File:** LightIdx.mul  

You can compute the following values from the Extra field in the index file.  

#### Sizes:

```csharp
short width = (extra & 0xFFFF);
short height = ((extra >> 16) & 0xFFFF);
```

#### Data:

```csharp
sbyte[width * height] additiveLightmap;
```

For each pixel (loaded top to bottom, left to right), add the lightmap value for each R, G, and B.  

As the value ranges from -31 to 31, you could easily apply a hue by using the color entry at Math.Abs( value ).  

**See Also:**  
 - [Index Files](indexes.md "Index")  
</span>
