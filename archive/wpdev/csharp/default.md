> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/default.html
> 

## Type Definitions

|Type|Definitions|
|-|-|
|char|Character 8-bit|
|sbyte|Signed 8-bit|
|byte|Unsigned 8-bit|
|short|Signed 16-bit|
|ushort|Unsigned 16-bit|
|int|Signed 32-bit|
|uint|Unsigned 32-bit|
|long|Signed 64-bit|
|ulong|Unsigned 64-bit|
|bool|Boolean 8-bit|

## Definitions

|Definitions||
|-|-|
|[Colors](colors.md)|Description of UO's 16-bit color values|
|[Index Files](indexes.md)|Format of the generic index file|
|[Anim.mul](animations.md)|Animated character/equip graphics|
|[Anim.idx](animations.md)|Index file for Anim.mul|
|[AnimData.mul](animdata.md)|Data for static animations|
|[Art.mul](art.md)|Land, static, and UO alpha graphics|
|[ArtIdx.mul](art.md)|Index file for Art.mul|
|[Fonts.mul](fonts.md)|Nine character sets|
|[GumpArt.mul](gumps.md)|Gump interface graphics|
|[GumpIdx.mul](gumps.md)|Index file for GumpArt.mul|
|Hues.mul|Hue coloring tables|
|[Light.mul](lights.md)|Additive lightmap graphics|
|[LightIdx.mul](lights.md)|Index file for Light.mul|
|[Map<n>.mul](maps.md)|Land Matrix|
|[MultiMap.rle](multimap.md)|Overhead Map View|
|[Multi.mul](multis.md)|Multi item lists|
|[Multi.idx](multis.md)|Index file for Multi.mul|
|[Palette.mul](palette.md)|24-bit RGB palette of unknown use|
|[RadarCol.mul](radarcol.md)|Tile->Color table|
|[SJIS2Uni.mul](sjis2uni.md)|Conversion table from SJIS to Unicode|
|[SkillGrp.mul](skillgroups.md)|Defines what skills are in which groups|
|[Skills.mul](skills.md)|Skill table|
|[Skills.idx](skills.md)|Index file for Skills.mul|
|[Sound.mul](sounds.md)|Sounds (PCM 22.050 kHz, 16-Bit Mono|
|[SoundIdx.mul](sounds.md)|Index file for Sound.mul|
|[Speech.mul](speech.md)|Keyword/ID table|
|[StaIdx<n>.mul](statics.md)|Index file for Statics.mul|
|[Statics<n>.mul](statics.md)|Static item lists|
|[TexIdx.mul](textures.md)|Index file for TexMaps.mul|
|[TexMaps.mul](textures.md)|Ground texture graphics|
|[TileData.mul](tiledata.md)|Data about tiles (flags, properties, name)|
|[UniFont\[n\].mul](unifont.md)|Unicode character set|
|[VerData.mul](verdata.md)|Patch data|
