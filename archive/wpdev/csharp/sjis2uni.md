> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/sjis2uni.html
> 

## SJIS To UniFont Table

**Data File:** SJIS2Uni.mul  

#### Data:

```csharp
 short[0x10000] table;
```

This file is used to convert an SJIS character to the proper UniFont[n].mul character graphic.  

**See Also:**  
 - [Unicode Fonts](unifont.md "Unifont")  
