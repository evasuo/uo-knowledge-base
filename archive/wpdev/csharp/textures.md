> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/textures.html
> 

## Textures

**Data File:** TexMaps.mul  
**Index File:** TexIdx.mul  

You can compute the width and height of each texture by the Extra value of the index:  

#### Sizes:

```csharp
int width = (extra == 1 ? 128 : 64);
int height = width;
```

#### Data:

```csharp
short[width * height] imageColors;
```

The imageColor array contains the image pixels loaded top to bottom, left to right.  

**See Also:**  
 - [UO Color Format](colors.md "Colors")  
 - [Index Files](indexes.md "Index")  
