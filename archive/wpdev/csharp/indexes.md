> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/index.html
> 

## Index

Index files are used to lookup data in other MUL files.  

#### Data:

```csharp
int lookup;
int length;
int extra;
```

To find data for a certain entry, you seek to (EntryID * 12), and read the data.  

Then, in the actual MUL file (not the index file), you seek to (lookup), and read (length) number of bytes.  

Some data relies on the extra field.  

