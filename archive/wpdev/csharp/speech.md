> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/speech.html
> 

## Speech

**Data File:** Speech.mul  

This file has no indexer. Directly after one chunk ends, another begins.  

#### Chunks:

```csharp
short keywordID; // Note: This is big-endian
short keywordLength; // Note: This is also big-endian
char[keywordLength] keyword;
```

When the client says a certain keyword, it only has to send the keyword ID to the server.  

As keywords can be in different languages, the server no longer needs to translate speech.  
