> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/multis.html
> 

## Multis

**Data File:** Multi.mul  
**Index File:** Multi.idx  

#### Data:

```csharp
List Items[length / 12]:
   short ItemID;
   short X;
   short Y;
   short Z;
   int isVisible; // 0=no, 1=yes
```

The center is at XYZ (0,0,0). The list needs to be sorted.  

**See Also:**  
 - [Index Files](indexes.md "Index")  
