> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/verdata.html
> 

## Version Data

**Data File:** VerData.mul  

#### Data:

```csharp
int patchCount;

List Patches[patchCount]:
   int FileID; // See below
   int BlockID; // Which block this patch overrides
   int lookup; // Patch data lookup
   int length; // Patch data length
   int extra; // Patch data extra

Patch data...
```

Patched data is in the same format of the original file. 

|FileID|File definitions|
|-|-|
|0x0|Map0.mul|
|0x1|StaIdx0.mul|
|0x2|Statics0.mul|
|0x3|ArtIdx.mul|
|0x4|Art.mul|
|0x5|Anim.idx|
|0x6|Anim.mul|
|0x7|SoundIdx.mul|
|0x8|Sound.mul|
|0x9|TexIdx.mul|
|0xA|TexMaps.mul|
|0xB|GumpIdx.mul|
|0xC|GumpArt.mul|
|0xD|Multi.idx|
|0xE|Multi.mul|
|0xF|Skills.idx|
|0x10|Skills.mul|
|0x1E|TileData.mul|
|0x1F|AnimData.mul|
