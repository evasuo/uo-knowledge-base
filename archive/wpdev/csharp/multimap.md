> 
> Archive of http://wpdev.sourceforge.net/docs/formats/csharp/multimap.html
> 

## Multi Map

**Data File:** MultiMap.rle  

This file contains an overhead view of the Britannia map.  

#### Data:

```csharp
int width;
int height;
Pixel Data......
```
 

This file contains an overhead view of the Britannia map. The image dimensions are defined by Width and Height. 

Pixels are either on or off.  

#### <a title="8-bits">Pixel Data</a>:

<table border="1">

<thead>

<tr>

<th>7</th>

<th>6</th>

<th>5</th>

<th>4</th>

<th>3</th>

<th>2</th>

<th>1</th>

<th>0</th>

</tr>
</thead>
<tbody>
<tr>

<td colspan="1" bgcolor="#C80000"><font color="#FFFFFF">On?</font></td>

<td colspan="7">Count</td>

</tr>

</tbody>

</table>

Then the on/off pixel is repeated Count number of times. 

If X exceeds width, set X to (X Modulus width)  
