# Ultima Online Binary File Grammar

To assist developers in understanding and troubleshooting the content of UO binary files such as `mul`, `idx`, and others, the Evasuo team is making available Grammar files (`.grammar`).

These files are compatible with [Synalyze It!](https://www.synalysis.net) (for Mac OS X) and [Hexinator](https://hexinator.com/) (for Windows & Linux).

| Sound.mul example | Map0.mul Example |
| - | - |
| ![Sound.mul Grammar](./examples/sound-mul--sound.mul.png) | ![Map0.mul Grammar](./examples/map-mul--map0.mul.png) |

## About Grammar Files

**File Type?**

`.grammar` files are stored in XML format

**How to use?**

1. Launch your `.grammar` compatible software ([Synalyze It!](https://www.synalysis.net) for Mac OS X / [Hexinator](https://hexinator.com/) for Windows & Linux). 
2. Load the file you wish to parse. For example, `sound.mul`
3. Load the grammar you wish to use. For example, `sound-mul.grammar`

You should now be able to use your grammar!

## UO Grammar Files

 - ANIM.IDX: [Generic UO Index File Grammar](./grammar/generic-uo-idx.grammar)
 - ANIM.MUL [Anim.mul File Grammar](./grammar/anim-mul.grammar)
 - ARTIDX.MUL: [Generic UO Index File Grammar](./grammar/generic-uo-idx.grammar)
 - ART.MUL
 - GUMPART.MUL
 - GUMPIDX.MUL: [GumpIdx.mul File Grammar](./grammar/gump-index-grammar.grammar)
 - HUES.MUL
 - MAP*.MUL: [Map File Grammar](./grammar/map-mul.grammar)
   - Parses `map*.mul` files.
   - For performance reasons, the current default is limited to 300 blocks. You can increase the limit by modifying the grammar file.
   - [Map0.mul Example](./examples/map-mul--map0.mul.png)
 - MULTI.IDX: [Generic UO Index File Grammar](./grammar/generic-uo-idx.grammar)
 - MULTI.MUL
 - PALETTE.MUL
 - RADARCOL.MUL
 - SKILLS.IDX: [Generic UO Index File Grammar](./grammar/generic-uo-idx.grammar)
 - SKILLS.MUL
 - SOUND.MUL: [Sound.mul File Grammar](./grammar/sound-mul.grammar)
   - [Sound.mul Example](./examples/sound-mul--sound.mul.png)
 - SOUNDIDX.MUL: [Generic UO Index File Grammar](./grammar/generic-uo-idx.grammar)
   - [Sound IDX Example](./examples/generic-uo-idx--soundidx.mul.png)
 - STAIDX*.MUL: [Generic UO Index File Grammar](./grammar/generic-uo-idx.grammar)
 - STATICS0.MUL
 - TILEDATA.MUL
 - TEXIDX.MUL: [Generic UO Index File Grammar](./grammar/generic-uo-idx.grammar)
 - TEXMAPS.MUL
 - VERDATA.MUL

