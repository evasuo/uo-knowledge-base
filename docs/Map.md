# Map*.mul

> **{TIP}** Before starting, you may want to learn about [hexadecimal and binary numbering systems](https://wiki.spherecommunity.net/index.php?title=Chapter_1) as and a little something about [bytes](https://en.wikipedia.org/wiki/Byte)

Map files are responsible for terrain rendering. Static tile data and positions are not included in these files.

The data within these files are grouped into a [matrix](https://en.wikipedia.org/wiki/Matrix_(mathematics)) of blocks. The matrix of blocks are loaded top-to-bottom and then left-to-right. `map0.mul` is a matrix of 512 blocks in height by 768 blocks in width. Each block consist of 64 cells. Unlike the parent blocks, the cells are loaded from *left-to-right* and then top-to-bottom.

To determine which block to render you will need to work from the X/Y player coordinates to get the x-block as well as the y-block. Once you have that, you can calculate the block number.

```javascript
// map0.mul height
const map0Height = 512

// Player Position
const playerXPosition = 1
const playerYPosition = 1

// Get the block matrix coordinates and use `>> 0` bitwise operator to ensure we return a proper 32-bit number
const xBlock = (playerXPosition / 8) >> 0
const yBlock = (playerYPosition / 8) >> 0

const blockNumber = (xBlock * map0Height) + yBlock

console.log(blockNumber) // 0
```

## Parsing

### Blocks

Total length: 196 bytes

UO generally uses little endian.

| length | description |
|-|-|
| 4 bytes | header - unknown. |
| 192 bytes | 64 [cells](#block_cells) |

### Block Cells

Total length: 3 bytes

| length | description |
|-|-|
| 16-bit integer (unsigned/little endian) | Art index tile ID. Lookup in artidx.mul with `artIdxuffer.slice(tildId * 12, 12)`. |
| 8-bit integer (unsigned/little endian) | altitude - range from -128 to 127 units above or below sea level.|
