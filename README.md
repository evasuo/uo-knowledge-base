# Evasuo's Ultima Online Resources for Developers

## Summary
- [Introduction](#introduction)
  - [What is Evasuo](#what-is-evasuo)
  - [What does "Evasuo" mean?](#what-does-evasuo-mean)
- [Client & Server Protocol](#client--server-protocol)
  - [Server-Sent Packet Implementations](#server-sent-packet-implementations)
  - [Client-Sent Packet Implementations](#client-sent-packet-implementations)
- [File Formats](#file-formats)

## Introduction

This repository exists to provide developers with all the information needed to understand the coding logic and functionality behind the Ultima Online gaming engine and servers.

This project intends to take advantage of the hard work put into [kevinhikaruevans/uojs2](https://github.com/kevinhikaruevans/uojs2) in order to extend, optimize and modernize the code so that a stable UO web client may be developed.

### What is Evasuo

[Evasuo](https://gitlab.com/evasuo/) is an open source initiative dedicated to developing the foundational tools and centralized knowledge-base required to enable an in-browser version of the Ultima Online client that will provide secure and stable gameplay within any modern web browser.

### What does "Evasuo" mean?
The term Evasuo (ĭ-vāˈz-ü-ō) is a play on the Latin word Evasio which can be translated to "deliverance" or "escape".

## Client & Server Protocol

This section links to resources providing information on UO packets and other protocol information for both the game engine and server.

- [NecroToolz - Kair Packet Guide](http://necrotoolz.sourceforge.net/kairpacketguide/)
  - Provides Server, Client and Obsolete packet information

### Server-Sent Packet Implementations

This sub-section of [Client & Server Protocol](#client--server-protocol) list resources providing examples of how servers are implmenting the packets that they are sending to the UO client.

- [SphereServer - Packet Sending](https://github.com/Sphereserver/Source-experimental/blob/master/src/network/send.cpp)
- [ServUO - Packet Handler](https://github.com/ServUO/ServUO/blob/master/Server/Network/PacketHandlers.cs)

### Client-Sent Packet Implementations

This sub-section of [Client & Server Protocol](#client--server-protocol) list resources providing examples of how servers process the packets that are being sent by the UO client.

- [SphereServer - Packet Receiving](https://github.com/Sphereserver/Source-experimental/blob/master/src/network/receive.cpp)
- [ServUO - Packet Handler](https://github.com/ServUO/ServUO/blob/master/Server/Network/PacketHandlers.cs)

## File Formats

- [Binary Grammar Config for Parsing UO Binary Files](./binary-grammar/readme.md)
- UO Stratics
  - [C#](https://uo.stratics.com/heptazane/fileformats.shtml) | [Modernized Archive](archive/heptazane/heptazane-file-formats.md)
- WP Dev - Outdated? Compare C# soundidx.mul to UO Stratics soundidx.mul docs. They have different requirements?
  - [C#](http://wpdev.sourceforge.net/docs/formats/csharp/default.html) | [Modernized Archive](archive/wpdev/csharp/default.md)
  - [VB](http://wpdev.sourceforge.net/docs/formats/vbasic/default.html)
  - [VB6](http://wpdev.sourceforge.net/docs/formats/vbasic6/default.html)
